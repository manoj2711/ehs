package com.cdac.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.cdac.dto.User;

@Component
@Repository
public class EHSDaoImple implements EHSDao  {
	
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void insert(User user) {
		
		String sql = "INSERT INTO USER (EmailId,UserName,Password,FlatNo,Mobile,RollId,Flag) VALUES (?,?,?,?,?,?,?)";
		jdbcTemplate.update(sql,user.getEmailId(),user.getUserName(),user.getPassword(),user.getFlatNo(),user.getMobile(),user.getRollId(),user.getFlag());
		
		
	}
	
	

}
