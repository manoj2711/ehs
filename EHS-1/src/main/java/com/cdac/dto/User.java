package com.cdac.dto;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Entity
@Table(name = "user")
public class User {
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int UserId;
	@Id
	private String EmailId;
	private String UserName;
	private String Password;
	private int FlatNo;
	private String Mobile;
	private int RollId=20;
	@Value("${some.key:true}")
	private Boolean Flag;
	
	public int getUserId() {
		return UserId;
	}
	public void setUserId(int userId) {
		UserId = userId;
	}
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public int getFlatNo() {
		return FlatNo;
	}
	public void setFlatNo(int flatNo) {
		FlatNo = flatNo;
	}
	public String getMobile() {
		return Mobile;
	}
	public void setMobile(String mobile) {
		Mobile = mobile;
	}
	public int getRollId() {
		return RollId;
	}
	public void setRollId(int rollId) {
		RollId = rollId;
	}
	public boolean getFlag() {
		return Flag;
	}
	public void setFlag(Boolean flag) {
		Flag = flag;
	}
	
		

}
